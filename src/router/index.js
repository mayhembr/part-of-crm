import Vue from 'vue'
import VueRouter from 'vue-router'
import Schedule from '../views/Schedule'
import Services from '../views/Services'
import Orders from '../views/Orders'
import Invoices from '../views/Invoices'

Vue.use(VueRouter)
const routes = [
  {
    path: '/',
    redirect: { name: 'services' }
  },
  {
    path: '/schedule',
    name: 'schedule',
    component: Schedule
  },
  {
    path: '/services/:serviceId',
    name: 'servicesId',
    component: Services
  },
  {
    path: '/services',
    name: 'services',
    component: Services
  },
  {
    path: '/orders',
    name: 'orders',
    component: Orders
  },
  {
    path: '/orders/:orderId',
    name: 'ordersId',
    component: Orders
  },
  {
    path: '/invoices',
    name: 'invoices',
    component: Invoices
  },
  {
    path: '/invoices/:childrenId',
    name: 'invoicesId',
    component: Invoices
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.VUE_APP_API_SUB_URL,
  routes
})

export default router
