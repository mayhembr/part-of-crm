import api from '../../api'
import { Product as ProductModel } from '../../models/Product.js'
import store from '../../store'
import messages from '../../utility/messagesNotifications'

export default {
  namespaced: true,
  state: {
    products: []
  },
  mutations: {
    SET_PRODUCTS(state, vals) {
      state.products = vals
    },
    ADD_PRODUCT(state, val) {
      // TODO
      // state.products.push(val)
    },
    UPDATE_PRODUCT(state, product) {
      // TODO
    },
    DELETE_PRODUCT(state, productsId) {
      // TODO
    }
  },
  actions: {
    async getProducts({ commit }) {
      try {
        const results = await api.products.get()
        const products = results.map(data => new ProductModel(data))
        console.info('products from api', products)
        commit('SET_PRODUCTS', products)
      } catch (err) {
        store.dispatch('notificationsModule/showNotification', { text: messages.errorGetProducts, type: 'error' })
        console.error(err)
      }
    },
    async saveProduct({ commit }, productData) {
      try {
        // получаем сохраненный продукт
        const dataSave = await api.products.add(ProductModel.reverseMappingForTariff(productData))
        const productSave = new ProductModel(dataSave)
        console.info('save product in api', productSave)
        // TODO добавлять к текущей услуге
        // commit('ADD_PRODUCT', productSave)
        // store.dispatch('notificationsModule/showNotification', { text: messages.saveProducts })
        return productSave
      } catch (err) {
        store.dispatch('notificationsModule/showNotification', { text: messages.errorSaveProducts, type: 'error' })
        console.error(err)
      }
    },
    async updateProduct({ commit }, productData) {
      try {
        // получаем обновленный продукт
        const dataUpdate = await api.products.update(ProductModel.reverseMappingForTariff(productData))
        const productUpdate = new ProductModel(dataUpdate)
        console.info('update product in api', productUpdate)
        // commit('UPDATE_PRODUCT', productUpdate)
        // store.dispatch('notificationsModule/showNotification', { text: messages.updateProduct })
        return productUpdate
      } catch (err) {
        store.dispatch('notificationsModule/showNotification', { text: messages.errorUpdateProduct, type: 'error' })
        console.error(err)
      }
    },
    async deleteProduct({ commit }, productsId) {
      try {
        await api.products.deleteProduct(productsId)
        console.info('delete products in api', productsId)
        // commit('DELETE_PRODUCT', productsId)
        // store.dispatch('notificationsModule/showNotification', { text: messages.deleteProduct })
        return productsId
      } catch (err) {
        store.dispatch('notificationsModule/showNotification', { text: messages.errorDeleteProduct, type: 'error' })
        console.error(err)
      }
    }
  }
}
