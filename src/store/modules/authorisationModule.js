import api from '../../api'
import { Account } from '../../models/Account'

export default {
  namespaced: true,
  state: {
    account: null,
    currentUsedCompany: null,
    token: null
  },
  mutations: {
    SET_TOKEN(state, token) {
      state.token = token
    },
    SET_ACCOUNT(state, account) {
      state.account = account
    },
    SET_USED_COMPANY(state, company) {
      state.currentUsedCompany = company
      console.log(`%ccurrent used company ${company}`, 'color: orange')
    }
  },
  actions: {
    async getToken({ commit }) {
      try {
        const localToken = process.env.VUE_APP_ACCESS_TOKEN
        const isUseLocalToken = process.env.VUE_APP_IS_USE_LOCAL_TOKEN === 'true'

        const token = isUseLocalToken ? localToken : await api.auth.getToken()
        console.log(`%c get token out of api ${token}`, 'color: green')
        commit('SET_TOKEN', token)
        return token
      } catch (err) {
        console.log('%c not token out of api', 'color: green')
        console.error(err)
      }
    },
    async getAccount({ commit }) {
      try {
        const results = await api.auth.getAccount()
        const account = new Account(results)
        console.info('account from api', account)
        commit('SET_ACCOUNT', account)
        if (account.companies.length > 0) commit('SET_USED_COMPANY', account.companies[0])
        return account
      } catch (err) {
        console.error(err)
      }
    }
  }
}
