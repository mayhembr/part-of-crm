import api from '../../api'
import { Place as PlaceModel } from '../../models/Place.js'
import store from '../../store'
import messages from '../../utility/messagesNotifications'
import { Order as OrderModel } from '../../models/Order'

export default {
  namespaced: true,
  state: {
    places: [],
    selectPlace: null
  },
  mutations: {
    SET_PLACES(state, vals) {
      state.places = vals
    },
    SET_SELECT_PLACE(state, place) {
      state.selectPlace = place
    },
    SET_PLACE(state, val) {
      state.places.push(val)
    },
    DELETE_PLACE(state, placeId) {
      const index = state.places.findIndex(pl => pl.id === placeId)
      if (index === -1) return
      state.places.splice(index, 1)
    },
    UPDATE_PLACE(state, place) {
      const index = state.places.findIndex(pl => pl.id === place.id)
      if (index === -1) return
      state.places.splice(index, 1, place)
    }
  },
  actions: {
    async getPlaces({ commit }) {
      try {
        const results = await api.places.get()
        const places = results.map(data => new PlaceModel(data))
        console.info('places from api', places)
        commit('SET_PLACES', places)
      } catch (err) {
        store.dispatch('notificationsModule/showNotification', { text: messages.errorGetPlace, type: 'error' })
        console.error(err)
      }
    },
    async savePlace({ commit }, placeData) {
      try {
        // получаем сохраненное место
        const dataSave = await api.places.add(PlaceModel.reverseMapping(placeData))
        const placeSave = new PlaceModel(dataSave)
        console.info('save place in api', placeSave)
        commit('SET_PLACE', placeSave)
        store.dispatch('notificationsModule/showNotification', { text: messages.addPlace })
      } catch (err) {
        store.dispatch('notificationsModule/showNotification', { text: messages.errorAddPlace, type: 'error' })
        console.error(err)
      }
    },
    async updatePlace({ commit }, placeData) {
      try {
        const dataUpdate = await api.places.update(PlaceModel.reverseMapping(placeData))
        const placeUpdate = new PlaceModel(dataUpdate)
        console.info('update place in api', placeUpdate)
        // обновляем его в сторе
        commit('UPDATE_PLACE', placeUpdate)
        store.dispatch('notificationsModule/showNotification', { text: messages.updatePlace })
        return placeUpdate
      } catch (err) {
        store.dispatch('notificationsModule/showNotification', { text: messages.errorUpdatePlace, type: 'error' })
        console.error(err)
      }
    },
    async deletePlace({ commit }, placeId) {
      try {
        await api.places.deletePlace(placeId)
        console.info('delete place in api', placeId)
        commit('DELETE_PLACE', placeId)
        store.dispatch('notificationsModule/showNotification', { text: messages.deletePlace })
      } catch (err) {
        store.dispatch('notificationsModule/showNotification', { text: messages.errorDeletePlace, type: 'error' })
        console.error(err)
      }
    }
  }
}
