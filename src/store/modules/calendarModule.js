export default {
  namespaced: true,
  state: {
    typesCalendar: {
      DAY: 'day',
      WEEK: 'week',
      FOUR_DAY: '4day',
      MONTH: 'month'
    },
    currentTypeCalendar: 'week',
    isTimeWork: true,
    focusDay: null,

    eventClickChangeDate: {}
  },
  mutations: {
    TOGGLE_TYPE_CALENDAR(state, type) {
      state.currentTypeCalendar = type
    },
    TOGGLE_IS_TIME_WORK(state, val) {
      state.isTimeWork = val
    },
    SET_FOCUS_DAY(state, day) {
      state.focusDay = day
    },
    EMIT_CLICK_CHANGE_DATE(state, { type, value }) {
      state.eventClickChangeDate = { type, value }
    }
  },
  actions: {
  }
}
