import api from '../../api'
import { Invoice as InvoiceModel, InvoiceItem, TYPES_STATUS } from '../../models/Invoice'
import store from '../../store'
import messages from '../../utility/messagesNotifications'
import { FinanceChildren as FinanceChildrenModel } from '../../models/Finance'

export default {
  namespaced: true,
  state: {
    selectInvoice: null,
    financesChildrens: [],
    currentChildrenId: null,
    invoices: [],
    searchText: ''
  },
  getters: {
    invoicesExceptCurrent: state => {
      if (!state.selectInvoice) return state.invoices
      return state.invoices.filter(i => i.id !== state.selectInvoice.id)
    },
    invoicesStatistics: state => {
      let allCount = 0
      let allAmount = 0
      let notPayedCount = 0
      let notPayedAmount = 0
      let payedCount = 0
      let payedAmount = 0
      state.financesChildrens.forEach(f => {
        allCount += f.invoiceCount
        allAmount += f.invoiceAmount
        notPayedCount += f.invoiceNotPayedCount
        notPayedAmount += f.invoiceNotPayedAmount
      })
      payedCount = allCount - notPayedCount
      payedAmount = allAmount - notPayedAmount
      return {
        allCount,
        allAmount: allAmount.toFixed(2),
        notPayedCount,
        notPayedAmount: notPayedAmount.toFixed(2),
        payedCount,
        payedAmount: payedAmount.toFixed(2)
      }
    },
    invoicesChildrenStatistics: state => {
      const allCount = state.invoices.length
      let allAmount = 0
      let notPayedCount = 0
      let notPayedAmount = 0
      let payedCount = 0
      let payedAmount = 0
      state.invoices.forEach(i => {
        allAmount += Number(i.amount)
        if (i.hasPayedAt) {
          payedCount++
          payedAmount += Number(i.amount)
        } else {
          notPayedCount++
          notPayedAmount += Number(i.amount)
        }
      })
      return {
        allCount,
        allAmount: allAmount.toFixed(2),
        notPayedCount,
        notPayedAmount: notPayedAmount.toFixed(2),
        payedCount,
        payedAmount: payedAmount.toFixed(2)
      }
    }
  },
  mutations: {
    SET_SEARCH_TEXT(state, val) {
      state.searchText = val
    },
    SET_FINANCES(state, vals) {
      state.financesChildrens = vals
    },
    SET_CURRENT_CHILDREN_ID(state, id) {
      state.currentChildrenId = id
    },
    SET_INVOICES(state, vals) {
      state.invoices = vals
    },
    ADD_INVOICE(state, val) {
      state.invoices.unshift(val)
    },
    SET_SELECT_INVOICE(state, inv) {
      state.selectInvoice = inv
    },
    DELETE_INVOICE(state, invoiceId) {
      const index = state.invoices.findIndex(inv => inv.id === invoiceId)
      if (index === -1) return
      state.invoices.splice(index, 1)
    },
    PAID_INVOICE(state, invoiceId) {
      const invoice = state.invoices.find(inv => inv.id === invoiceId)
      if (!invoice) return
      invoice.status = TYPES_STATUS.Paid.value
    },
    SEND_INVOICE(state, invoiceId) {
      const invoice = state.invoices.find(inv => inv.id === invoiceId)
      if (!invoice) return
      // TODO
      // invoice.status = TYPES_STATUS.Paid.value
    },
    SET_INVOICE_ITEM(state, invoiceItem) {
      // перенос
      const indexInvoices = state.invoices.findIndex(inv => inv.id === invoiceItem.invoice)
      if (indexInvoices === -1) return
      state.invoices[indexInvoices].items.push(invoiceItem)
    }
  },
  actions: {
    async saveItem({ commit }, item) {
      try {
        item = InvoiceItem.reverseMapping(item)
        const results = await api.invoices.addItem(item)
        const invoiceItem = new InvoiceItem(results)
        console.info('save for InvoiceItem from api', invoiceItem)
        // commit('SET_INVOICE_ITEM', invoiceItem)
        store.dispatch('notificationsModule/showNotification', { text: messages.saveInvoiceItem })
        return invoiceItem
      } catch (err) {
        store.dispatch('notificationsModule/showNotification', { text: messages.errorSaveInvoiceItem, type: 'error' })
        console.error(err)
      }
    },
    async updateItems({ commit }, item) {
      try {
        item = InvoiceItem.reverseMapping(item)
        const results = await api.invoices.updateItem(item)
        const invoiceItem = new InvoiceItem(results)
        console.info('update for InvoiceItem from api', invoiceItem)
        // commit('SET_INVOICE_ITEM', invoiceItem)
        return invoiceItem
      } catch (err) {
        store.dispatch('notificationsModule/showNotification', { text: messages.errorUpdateInvoiceItem, type: 'error' })
        console.error(err)
      }
    },
    async deleteItem({ commit }, itemId) {
      try {
        await api.invoices.deleteItem(itemId)
        console.info('delete item in api', itemId)
        // commit('DELETE_INVOICE', invoiceId)
        // store.dispatch('notificationsModule/showNotification', { text: messages.deleteItem })
      } catch (err) {
        store.dispatch('notificationsModule/showNotification', { text: messages.errorDeleteItem, type: 'error' })
        console.error(err)
      }
    },
    async getFinancesChildrens({ commit }) {
      try {
        const results = await api.finances.get()
        const finances = results.map(data => new FinanceChildrenModel(data))
        console.info('finances for children from api', finances)
        commit('SET_FINANCES', finances)
      } catch (err) {
        store.dispatch('notificationsModule/showNotification', { text: messages.errorGetFinances, type: 'error' })
        console.error(err)
      }
    },
    async getInvoicesForChildren({ commit, state }) {
      try {
        if (!state.currentChildrenId) return
        const results = await api.finances.getForChildren(state.currentChildrenId)
        const invoices = results.map(data => new InvoiceModel(data))
        // const invoices = results
        console.info('invoices finance for children from api', invoices)
        commit('SET_INVOICES', invoices)
      } catch (err) {
        store.dispatch('notificationsModule/showNotification', { text: messages.errorGetFinanceChildren, type: 'error' })
        console.error(err)
      }
    },
    async paidInvoices({ commit }, invoiceId) {
      try {
        await api.invoices.paid({ invoice_id: invoiceId })
        console.info('paid invoice in api', invoiceId)
        commit('PAID_INVOICE', invoiceId)
        store.dispatch('notificationsModule/showNotification', { text: messages.paidInvoice })
      } catch (err) {
        store.dispatch('notificationsModule/showNotification', { text: messages.errorPaidInvoice, type: 'error' })
        console.error(err)
      }
    },
    async sendInvoice({ commit }, invoiceId) {
      try {
        await api.invoices.send(invoiceId)
        console.info('send invoice in api', invoiceId)
        commit('SEND_INVOICE', invoiceId)
        store.dispatch('notificationsModule/showNotification', { text: messages.sendInvoice })
      } catch (err) {
        store.dispatch('notificationsModule/showNotification', { text: messages.errorSendInvoice, type: 'error' })
        console.error(err)
      }
    },
    async getInvoices({ commit }) {
      try {
        const results = await api.invoices.get()
        const invoices = results.map(data => new InvoiceModel(data))
        console.info('invoices from api', invoices)
        commit('SET_INVOICES', invoices)
      } catch (err) {
        store.dispatch('notificationsModule/showNotification', { text: messages.errorGetInvoices, type: 'error' })
        console.error(err)
      }
    },
    async saveInvoices({ commit }, invoiceData) {
      try {
        // получаем сохраненный заказ
        const dataSave = await api.invoices.add(InvoiceModel.reverseMapping(invoiceData))
        const invoiceSave = new InvoiceModel(dataSave)
        console.info('save invoice in api', invoiceSave)
        // добовляем его к другим
        commit('ADD_INVOICE', invoiceSave)
        store.dispatch('notificationsModule/showNotification', { text: messages.saveInvoice })
        return invoiceSave
      } catch (err) {
        store.dispatch('notificationsModule/showNotification', { text: messages.errorSaveInvoice, type: 'error' })
        console.error(err)
      }
    },
    async updateInvoices({ commit }, invoiceData) {
      try {
        // получаем сохраненный заказ
        const dataUpdate = await api.invoices.update(InvoiceModel.reverseMapping(invoiceData))
        const invoiceUpdate = new InvoiceModel(dataUpdate)
        console.info('update invoice in api', invoiceUpdate)
        // добовляем его к другим
        // commit('UPDATE_INVOICE', invoiceUpdate)
        store.dispatch('notificationsModule/showNotification', { text: messages.updateInvoice })
        return invoiceUpdate
      } catch (err) {
        store.dispatch('notificationsModule/showNotification', { text: messages.errorUpdateInvoice, type: 'error' })
        console.error(err)
      }
    },
    async deleteInvoice({ commit }, invoiceId) {
      try {
        await api.invoices.deleteInvoice(invoiceId)
        console.info('delete invoice in api', invoiceId)
        // commit('DELETE_INVOICE', invoiceId)
        // store.dispatch('notificationsModule/showNotification', { text: messages.deleteInvoice })
      } catch (err) {
        store.dispatch('notificationsModule/showNotification', { text: messages.errorDeleteInvoice, type: 'error' })
        console.error(err)
      }
    }
  }
}
