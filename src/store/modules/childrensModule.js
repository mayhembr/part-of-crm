import api from '../../api'
import { ChildrenChip, ChildrenChipAttendance as ChildrenAttendanceModel } from '../../models/ChildrenAttendance.js'
import store from '../../store'
import messages from '../../utility/messagesNotifications'

export default {
  namespaced: true,
  state: {
    groupsChildrens: [],
    childrensForEvent: []
  },
  mutations: {
    SET_GROUPED_CHILDRENS(state, vals) {
      state.groupsChildrens = vals
    },
    SET_CHILDRENS_FOR_SELECT_EVENT(state, vals) {
      state.childrensForEvent = vals
    }
  },
  actions: {
    async getGroupsChildrens({ commit }) {
      try {
        const groupsChildrens = await api.childrens.getGrouped()
        groupsChildrens.forEach(group => {
          group.children = group.children.map(ch => new ChildrenChip(ch))
        })
        console.info('groups childrens chip from api', groupsChildrens)
        commit('SET_GROUPED_CHILDRENS', groupsChildrens)
      } catch (err) {
        store.dispatch('notificationsModule/showNotification', { text: messages.errorGetGroupChildrens, type: 'error' })
        console.error(err)
      }
    },
    async getChildrensForSelectEvent({ commit }) {
      try {
        // id для эвента берем из выбранного эвента
        const idSelectEvent = store.state.eventsModule.selectEvent.id
        const results = await api.events.getChildren(idSelectEvent)
        const childrens = results.map(data => new ChildrenAttendanceModel(data))
        console.info('childrens for select event from api', childrens)
        commit('SET_CHILDRENS_FOR_SELECT_EVENT', childrens)
      } catch (err) {
        store.dispatch('notificationsModule/showNotification', { text: messages.errorGetChildrensForEvent, type: 'error' })
        console.error(err)
      }
    },
    async saveChildrenAttendance({ commit }, attendanceData) {
      try {
        const results = await api.events.saveAttendance(ChildrenAttendanceModel.mappingForSaveAttendance(attendanceData))
        console.info('Attendance children save in api', results)
        // store.dispatch('notificationsModule/showNotification', { text: messages.saveAttendance })
      } catch (err) {
        store.dispatch('notificationsModule/showNotification', { text: messages.errorSaveAttendance, type: 'error' })
        console.error(err)
      }
    },
    async updateChildrenAttendance({ commit }, attendanceData) {
      try {
        const results = await api.events.updateAttendance(ChildrenAttendanceModel.mappingForUpdateAttendance(attendanceData))
        console.info('Attendance children update in api', results)
        // store.dispatch('notificationsModule/showNotification', { text: messages.updateAttendance })
      } catch (err) {
        store.dispatch('notificationsModule/showNotification', { text: messages.errorUpdateAttendance, type: 'error' })
        console.error(err)
      }
    }
  }
}
