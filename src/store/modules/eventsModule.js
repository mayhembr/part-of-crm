import api from '../../api'
import { Event as EventModel } from '../../models/Event.js'
import store from '../../store'
import messages from '../../utility/messagesNotifications'

export default {
  namespaced: true,
  state: {
    events: [],
    selectEvent: null,
    typeAttendance: [
      {
        name: 'absent',
        value: 'Was absent',
        text: 'Отсутствовал',
        color: 'red'
      },
      {
        name: 'absentDueIllness',
        value: 'Absent due to illness',
        text: 'По болезни',
        color: 'purple accent-3'
      },
      {
        name: 'absentForGoodReason',
        value: 'Absent for a good reason',
        text: 'Отсутствовал по уважительной причине',
        color: 'amber darken-1'

      },
      {
        name: 'attended',
        value: 'Was present',
        text: 'Присутствовал',
        color: 'light-blue lighten-3'
      }
    ]
  },
  mutations: {
    SET_EVENTS(state, vals) {
      state.events = vals
    },
    ADD_EVENT(state, val) {
      state.events.push(val)
    },
    UPDATE_EVENT(state, event) {
      const index = state.events.findIndex(evt => evt.id === event.id)
      if (index === -1) return
      state.events.splice(index, 1, event)
    },
    DELETE_EVENT(state, idEvent) {
      const index = state.events.findIndex(evt => evt.id === idEvent)
      if (index === -1) return
      state.events.splice(index, 1)
    },
    SET_SELECT_EVENT(state, evt) {
      state.selectEvent = evt
    }
  },
  actions: {
    async getEvents({ commit }) {
      try {
        const results = await api.events.get()
        const events = results.map(data => new EventModel(data))
        console.info('events from api', events)
        commit('SET_EVENTS', events)
      } catch (err) {
        store.dispatch('notificationsModule/showNotification', { text: messages.errorGetLesson, type: 'error' })
        console.error(err)
      }
    },
    async saveEvent({ commit }, eventData) {
      try {
        // получаем сохраненный эвент
        const dataSave = await api.events.add(EventModel.reverseMapping(eventData))
        const eventSave = new EventModel(dataSave)
        console.info('save event in api', eventSave)
        // добовляем его к другим
        commit('ADD_EVENT', eventSave)
        // store.dispatch('notificationsModule/showNotification', { text: messages.addLesson })
      } catch (err) {
        store.dispatch('notificationsModule/showNotification', { text: messages.errorAddLesson, type: 'error' })
        console.error(err)
      }
    },
    async updateEvent({ commit }, eventData) {
      try {
        // получаем обновленный эвент
        const dataUpdate = await api.events.update(EventModel.reverseMapping(eventData))
        const eventUpdate = new EventModel(dataUpdate)
        console.info('update event in api', eventUpdate)
        // обновляем его в сторе
        commit('UPDATE_EVENT', eventUpdate)
        // установить его текушем
        // commit('SET_SELECT_EVENT', eventUpdate)
        store.dispatch('notificationsModule/showNotification', { text: messages.updateLesson })
      } catch (err) {
        store.dispatch('notificationsModule/showNotification', { text: messages.errorUpdateLesson, type: 'error' })
        console.error(err)
      }
    },
    async deleteEvent({ commit }, idEvent) {
      try {
        await api.events.deleteEvent(idEvent)
        console.info('delete event in api', idEvent)
        commit('DELETE_EVENT', idEvent)
        store.dispatch('notificationsModule/showNotification', { text: messages.deleteLesson })
      } catch (err) {
        store.dispatch('notificationsModule/showNotification', { text: messages.errorDeleteLesson, type: 'error' })
        console.error(err)
      }
    }
  }
}
