import api from '../../api'
import { Order as OrderModel } from '../../models/Order.js'
import store from '../../store'
import messages from '../../utility/messagesNotifications'
import { Discount as DiscountModel } from '../../models/Discount'

export default {
  namespaced: true,
  state: {
    orders: [],
    selectOrder: null,
    searchText: ''
  },
  mutations: {
    SET_SEARCH_TEXT(state, val) {
      state.searchText = val
    },
    SET_ORDERS(state, vals) {
      state.orders = vals
    },
    SET_SELECT_ORDER(state, ord) {
      state.selectOrder = ord
    },
    ADD_ORDER(state, val) {
      state.orders.unshift(val)
    },
    UPDATE_ORDER(state, order) {
      const index = state.orders.findIndex(ord => ord.id === order.id)
      if (index === -1) return
      state.orders.splice(index, 1, order)
    },
    DELETE_ORDER(state, orderId) {
      const index = state.orders.findIndex(ord => ord.id === orderId)
      if (index === -1) return
      state.orders.splice(index, 1)
    },
    ADD_DISCOUNT_IN_ORDER(state, discount) {
      const index = state.orders.findIndex(ord => ord.id === discount.orderId)
      if (index === -1) return
      state.orders[index].discounts.push(discount)
    },
    UPDATE_DISCOUNT_IN_ORDER(state, discount) {
      const index = state.orders.findIndex(ord => ord.id === discount.orderId)
      if (index === -1) return
      const order = state.orders[index]
      const indexDiscounts = order.discounts.findIndex(di => di.id === discount.id)
      // если нашли то обновляем, если нет то добавляем
      if (indexDiscounts === -1) order.discounts.push(discount)
      else order.discounts.splice(indexDiscounts, 1, discount)
    },
    DELETE_DISCOUNT_IN_ORDER(state, discount) {
      const index = state.orders.findIndex(ord => ord.id === discount.orderId)
      if (index === -1) return
      const order = state.orders[index]
      const indexDiscounts = order.discounts.findIndex(di => di.id === discount.id)
      if (indexDiscounts === -1) return
      order.discounts.splice(indexDiscounts, 1)
    }
  },
  actions: {
    async getOrders({ commit }) {
      try {
        const results = await api.orders.get()
        const orders = results.map(data => new OrderModel(data))
        console.info('orders from api', orders)
        commit('SET_ORDERS', orders)
      } catch (err) {
        store.dispatch('notificationsModule/showNotification', { text: messages.errorGetOrders, type: 'error' })
        console.error(err)
      }
    },
    async saveOrder({ commit }, orderData) {
      try {
        // получаем сохраненный заказ
        const dataSave = await api.orders.add(OrderModel.reverseMappingForTariff(orderData))
        const orderSave = new OrderModel(dataSave)
        console.info('save order in api', orderSave)
        // добовляем его к другим
        commit('ADD_ORDER', orderSave)
        // store.dispatch('notificationsModule/showNotification', { text: messages.saveOrder })
        return orderSave
      } catch (err) {
        store.dispatch('notificationsModule/showNotification', { text: messages.errorSaveOrder, type: 'error' })
        console.error(err)
      }
    },
    async notifySaveOrders({ commit }, orders) {
      if (!orders) return
      let success = 0

      orders.forEach(ord => {
        if (ord) success++
      })
      store.dispatch('notificationsModule/showNotification', { text: `${messages.saveOrders} ${success} из ${orders.length}` })
    },
    async updateOrder({ commit }, orderData) {
      try {
        // получаем обновленный заказ
        const dataUpdate = await api.orders.update(OrderModel.reverseMappingForTariff(orderData))
        const orderUpdate = new OrderModel(dataUpdate)
        console.info('update order in api', orderUpdate)
        // обновляем его в сторе
        commit('UPDATE_ORDER', orderUpdate)
        store.dispatch('notificationsModule/showNotification', { text: messages.updateOrder })
        return orderUpdate
      } catch (err) {
        store.dispatch('notificationsModule/showNotification', { text: messages.errorUpdateOrder, type: 'error' })
        console.error(err)
      }
    },
    async deleteOrder({ commit }, orderId) {
      try {
        await api.orders.deleteOpder(orderId)
        console.info('delete order in api', orderId)
        commit('DELETE_ORDER', orderId)
        store.dispatch('notificationsModule/showNotification', { text: messages.deleteOrder })
      } catch (err) {
        store.dispatch('notificationsModule/showNotification', { text: messages.errorDeleteOrder, type: 'error' })
        console.error(err)
      }
    },
    async saveDiscount({ commit }, discountData) {
      try {
        // получаем сохраненную скидку
        const dataSave = await api.orders.addDiscount(DiscountModel.reverseMapping(discountData))
        const discountSave = new DiscountModel(dataSave)
        console.info('save Discount in api', discountSave)
        // добовляем его к другим
        commit('ADD_DISCOUNT_IN_ORDER', discountSave)
        // store.dispatch('notificationsModule/showNotification', { text: messages.saveDiscount })
        return discountSave
      } catch (err) {
        store.dispatch('notificationsModule/showNotification', { text: messages.errorSaveDiscount, type: 'error' })
        console.error(err)
      }
    },
    async updateDiscount({ commit }, discountData) {
      try {
        // получаем обновленную скидку
        const dataUpdate = await api.orders.updateDiscount(DiscountModel.reverseMapping(discountData))
        const discountUpdate = new DiscountModel(dataUpdate)
        console.info('Update Discount in api', discountUpdate)
        // обновляем его в сторе
        commit('UPDATE_DISCOUNT_IN_ORDER', discountUpdate)
        // store.dispatch('notificationsModule/showNotification', { text: messages.updateDiscount })
        return discountUpdate
      } catch (err) {
        store.dispatch('notificationsModule/showNotification', { text: messages.errorUpdateDiscount, type: 'error' })
        console.error(err)
      }
    },
    async deleteDiscount({ commit }, discount) {
      try {
        await api.orders.deleteDiscount(discount.id)
        console.info('delete discount in api', discount)
        commit('DELETE_DISCOUNT_IN_ORDER', discount)
        // store.dispatch('notificationsModule/showNotification', { text: messages.deleteDiscount })
      } catch (err) {
        store.dispatch('notificationsModule/showNotification', { text: messages.errorDeleteDiscount, type: 'error' })
        console.error(err)
      }
    }
  }
}
