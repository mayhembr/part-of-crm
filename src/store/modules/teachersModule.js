import api from '../../api'
import { TeacherChip } from '../../models/Teacher.js'
import store from '../../store'
import messages from '../../utility/messagesNotifications'

export default {
  namespaced: true,
  state: {
    teachers: []
  },
  mutations: {
    SET_TEACHERS(state, vals) {
      state.teachers = vals
    }

  },
  actions: {
    async getTeachers({ commit }) {
      try {
        const results = await api.teachers.get()
        const teachers = results.map(data => new TeacherChip(data))
        console.info('teachers chip from api', teachers)
        commit('SET_TEACHERS', teachers)
      } catch (err) {
        store.dispatch('notificationsModule/showNotification', { text: messages.errorGetTeachers, type: 'error' })
        console.error(err)
      }
    }
  }
}
