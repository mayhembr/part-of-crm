export default {
  namespaced: true,
  state: {
    modesModal: {
      ADD: 'ADD',
      COPY: 'COPY',
      UPDATE: 'UPDATE'
    },
    // page schedule
    isShowAddAuditorium: false,
    currentModeAddAuditorium: 'ADD',
    isShowLesson: false,
    currentModeModalLesson: 'ADD',
    isShowCheckStudent: false,
    isShowInfoEvent: false,
    // page services
    isShowInfoService: false,
    isShowChangeService: false,
    currentModeModalChangeService: 'ADD',
    // page orders
    isShowInfoOrder: false,
    isShowChangeOrder: false,
    currentModeModalChangeOrder: 'ADD'
  },
  mutations: {
    // page schedule
    TOGGLE_SHOW_ADD_AUDITORIUM(state, isShow) {
      state.isShowAddAuditorium = isShow
      if (!isShow) state.currentModeAddAuditorium = state.modesModal.ADD
    },
    TOGGLE_MODE_ADD_AUDITORIUM(state, mode) {
      state.currentModeAddAuditorium = mode
    },
    TOGGLE_SHOW_LESSON(state, isShow) {
      state.isShowLesson = isShow
      if (!isShow) state.currentModeModalLesson = state.modesModal.ADD
    },
    TOGGLE_MODE_LESSON(state, mode) {
      state.currentModeModalLesson = mode
    },
    TOGGLE_SHOW_CHECK_STUDENT(state, isShow) {
      state.isShowCheckStudent = isShow
    },
    TOGGLE_SHOW_INFO_EVENT(state, isShow) {
      state.isShowInfoEvent = isShow
    },
    // page services
    TOGGLE_SHOW_INFO_SERVICE(state, isShow) {
      state.isShowInfoService = isShow
    },
    TOGGLE_SHOW_CHANGE_SERVICE(state, isShow) {
      state.isShowChangeService = isShow
      if (!isShow) state.currentModeModalChangeService = state.modesModal.ADD
    },
    TOGGLE_MODE_CHANGE_SERVICE(state, mode) {
      state.currentModeModalChangeService = mode
    },
    // page orders
    TOGGLE_SHOW_INFO_ORDER(state, isShow) {
      state.isShowInfoOrder = isShow
    },
    TOGGLE_SHOW_CHANGE_ORDER(state, isShow) {
      state.isShowChangeOrder = isShow
      if (!isShow) state.currentModeModalChangeOrder = state.modesModal.ADD
    },
    TOGGLE_MODE_CHANGE_ORDER(state, mode) {
      state.currentModeModalChangeOrder = mode
    }
  },
  actions: {
  }
}
