import api from '../../api'
import { Service as ServiceModel } from '../../models/Service.js'
import store from '../../store'
import messages from '../../utility/messagesNotifications'

export default {
  namespaced: true,
  state: {
    services: [],
    selectService: null,
    searchText: ''
  },
  mutations: {
    SET_SEARCH_TEXT(state, val) {
      state.searchText = val
    },
    SET_SERVICES(state, vals) {
      state.services = vals
    },
    ADD_SERVICE(state, val) {
      state.services.unshift(val)
    },
    ADD_PRODUCT_IN_SERVICE(state, { serviceId, product }) {
      const index = state.services.findIndex(ser => ser.id === serviceId)
      if (index === -1) return
      state.services[index].products.push(product)
    },
    SET_SELECT_SERVICE(state, evt) {
      state.selectService = evt
    },
    UPDATE_SERVICE(state, service) {
      const index = state.services.findIndex(ser => ser.id === service.id)
      if (index === -1) return
      state.services.splice(index, 1, service)
    },
    UPDATE_PRODUCT_IN_SERVICE(state, { serviceId, product }) {
      const index = state.services.findIndex(ser => ser.id === serviceId)
      if (index === -1) return
      const service = state.services[index]
      const indexProduct = service.products.findIndex(pr => pr.id === product.id)
      // если нашли то обновляем, если нет то добавляем
      if (indexProduct === -1) service.products.push(product)
      else service.products.splice(indexProduct, 1, product)
    },
    DELETE_SERVICE(state, serviceId) {
      const index = state.services.findIndex(ser => ser.id === serviceId)
      if (index === -1) return
      state.services.splice(index, 1)
    },
    DELETE_PRODUCT_IN_SERVICE(state, { serviceId, product }) {
      const index = state.services.findIndex(ser => ser.id === serviceId)
      if (index === -1) return
      const service = state.services[index]
      const indexProduct = service.products.findIndex(pr => pr.id === product.id)
      if (index === -1) return
      service.products.splice(indexProduct, 1)
    }
  },
  actions: {
    async getServices({ commit }, companyId = null) {
      try {
        const results = await api.services.get(companyId)
        const services = results.map(data => new ServiceModel(data))
        console.info('services from api', services)
        commit('SET_SERVICES', services)
      } catch (err) {
        store.dispatch('notificationsModule/showNotification', { text: messages.errorGetServices, type: 'error' })
        console.error(err)
      }
    },
    async saveService({ commit }, servicesData) {
      try {
        // получаем сохраненную услугу
        const dataSave = await api.services.add(ServiceModel.reverseMapping(servicesData))
        const serviceSave = new ServiceModel(dataSave)
        console.info('save service in api', serviceSave)
        // добовляем его к другим
        commit('ADD_SERVICE', serviceSave)
        store.dispatch('notificationsModule/showNotification', { text: messages.addService })
        return serviceSave
      } catch (err) {
        store.dispatch('notificationsModule/showNotification', { text: messages.errorSaveService, type: 'error' })
        console.error(err)
      }
    },
    async updateService({ commit }, servicesData) {
      try {
        // получаем обновленную услугу
        const dataUpdate = await api.services.update(ServiceModel.reverseMapping(servicesData))
        const serviceUpdate = new ServiceModel(dataUpdate)
        console.info('update service in api', serviceUpdate)
        // обновляем его в сторе
        commit('UPDATE_SERVICE', serviceUpdate)
        store.dispatch('notificationsModule/showNotification', { text: messages.updateService })
        return serviceUpdate
      } catch (err) {
        store.dispatch('notificationsModule/showNotification', { text: messages.errorUpdateService, type: 'error' })
        console.error(err)
      }
    },
    async deleteService({ commit }, serviceId) {
      try {
        await api.services.deleteService(serviceId)
        console.info('delete Service in api', serviceId)
        commit('DELETE_SERVICE', serviceId)
        store.dispatch('notificationsModule/showNotification', { text: messages.deleteService })
      } catch (err) {
        store.dispatch('notificationsModule/showNotification', { text: messages.errorDeleteService, type: 'error' })
        console.error(err)
      }
    }
  }
}
