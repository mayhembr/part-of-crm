import Vue from 'vue'
import Vuex from 'vuex'
import authorisationModule from './modules/authorisationModule'
import modalsModule from './modules/modalsModule'
import calendarModule from './modules/calendarModule'
import eventsModule from './modules/eventsModule'
import servicesModule from './modules/servicesModule'
import placesModule from './modules/placesModule'
import teachersModule from './modules/teachersModule'
import notificationsModule from './modules/notificationsModule'
import childrensModule from './modules/childrensModule'
import productsModule from './modules/productsModule'
import ordersModule from './modules/ordersModule'
import invoicesModule from './modules/invoicesModule'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    authorisationModule,
    modalsModule,
    calendarModule,
    eventsModule,
    servicesModule,
    placesModule,
    teachersModule,
    notificationsModule,
    childrensModule,
    productsModule,
    ordersModule,
    invoicesModule
  }
})
