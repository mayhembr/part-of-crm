import { isEmpty } from '../utility/helpers'
import { Order } from './Order'
import { Children } from './Children'

export class InvoiceItem {
  constructor(data) {
    this.id = data.id
    this.order = typeof data.order === 'object' && data.order ? new Order(data.order) : data.order
    this.service = data.service
    this.description = data.description
    this.invoice = data.invoice
    this.count = data.count
    this.price = data.price
    this.discount = data.discount
    this.currency = data.currency
    this.compensation = data.compensation
  }

  get finalCost() {
    try {
      const res = +this.price - +this.discount
      return +res.toFixed(2)
    } catch (error) {
      return 0
    }
  }

  static reverseMapping(data) {
    const keys = Object.keys(data)
    // заменяем название полей
    const obj = keys.reduce((acc, key) => {
      // если нет в map то не добавляем такое поле
      if (!mapAllInvoiceItem[key]) return acc
      // только для не пустых полей
      if (!isEmpty(data[key])) acc[mapAllInvoiceItem[key]] = data[key]
      return acc
    }, {})
    return obj
  }
}
const mapAllInvoiceItem = {
  id: 'id',
  order: 'order',
  invoice: 'invoice',
  service: 'service',
  description: 'description',
  count: 'count',
  price: 'price',
  discount: 'discount',
  currency: 'currency',
  compensation: 'compensation'
}

export class Invoice {
  constructor(data) {
    // Object.assign(this, data)
    this.id = data.id
    this.createdAt = data.created_at
    this.company = data.company
    // проверка на null
    this.recipient = typeof data.recipient === 'object' && data.recipient ? new Children(data.recipient) : data.recipient
    this.comment = data.comment
    this.num = data.num
    this.series = data.series
    this.IsAutoCreated = data.is_auto_created
    this.period = data.period
    this.dateFrom = data.date_from
    this.payDate = data.pay_date
    this.customerId = data.customer
    this.amount = data.amount
    this.currency = data.currency
    this.isNotified = data.is_notified
    this.status = data.status && TYPES_STATUS[data.status].value
    this.hasPayedAt = data.has_payed_at
    this.hasPayed = data.has_payed
    this.hasSentAt = data.has_sent_at
    this.pdfLink = data.pdf_link
    this.items = data.items ? data.items.map(d => new InvoiceItem(d)) : []

    this.isHasPartialPayment = data.has_partial_payment
    this.partialPayment = data.partial_payment
  }

  static reverseMapping(data) {
    const keys = Object.keys(data)
    // заменяем название полей
    const obj = keys.reduce((acc, key) => {
      // если нет в map то не добавляем такое поле
      if (!mapAll[key]) return acc
      // только для не пустых полей
      if (!isEmpty(data[key])) acc[mapAll[key]] = data[key]
      return acc
    }, {})
    return obj
  }
}

export const TYPES_STATUS = {
  Draft: {
    value: 'Draft',
    text: 'Черновик',
    color: '#E5E5E5'
  },
  'Ready to ship': {
    value: 'Ready to ship',
    text: 'Готов к отправке',
    color: '#045c21'
  },
  Sent: {
    value: 'Sent',
    text: 'Отправлен',
    color: '#E79C71'
  },
  Paid: {
    value: 'Paid',
    text: 'Оплачен',
    color: '#76CBC6'
  },
  Resubmitted: {
    value: 'Resubmitted',
    text: ' Отправлен повторно',
    color: '#F27B37'
  },
  Canceled: {
    value: 'Canceled',
    text: 'Отменен',
    color: '#282A2E'
  },
  Error: {
    value: 'Error',
    text: 'Отказ от оплаты',
    color: '#EC5252'
  }
}

export const TYPES_PAYED_STATUS = {
  payed: {
    value: 'payed',
    text: 'Оплачен'
  },
  notPayed: {
    value: 'notPayed',
    text: 'Не оплачен'
  },
  partial: {
    value: 'partial',
    text: 'Частичная оплата'
  }
}

const mapAll = {
  id: 'id',
  company: 'company',
  recipientId: 'recipient',
  comment: 'start_at',
  IsAutoCreated: 'is_auto_created',
  period: 'period',
  dateFrom: 'date_from',
  payDate: 'pay_date',
  customerId: 'customer',
  amount: 'amount',
  currency: 'currency',
  isNotified: 'is_notified',
  status: 'status',
  series: 'series',
  hasPayedAt: 'has_payed_at',
  hasPayed: 'has_payed',
  hasSentAt: 'has_sent_at',
  items: 'items',
  isHasPartialPayment: 'has_partial_payment',
  partialPayment: 'partial_payment'
}
