import { Account } from './Account'
import { Invoice } from './Invoice'
// import { DateFormatter } from './DateFormatter'

export class FinanceChildren {
  constructor(data) {
    // Object.assign(this, data)
    this.id = data.id
    this.fullName = data.full_name
    this.birthDay = data.birth_day
    this.groupName = data.group_name

    this.family = data.family ? data.family.map(a => new Account(a)) : []
    this.lastInvoice = data.last_invoice ? new Invoice(data.last_invoice) : null

    this.invoiceCount = data.invoice_count
    this.invoiceAmount = data.invoice_amount
    this.invoiceNotPayedCount = data.invoice_not_payed_count
    this.invoiceNotPayedAmount = data.invoice_not_payed_amount
  }
}
