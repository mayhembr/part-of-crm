import { Product } from './Product'
import { DateFormatter } from './DateFormatter'
import { isEmpty } from '../utility/helpers'

export class Service extends DateFormatter {
  constructor(data) {
    super(data.created_at)
    this.id = data.id
    this.name = data.name
    this.description = data.description || ''
    this.ageGroup = data.age_group || []
    this.avatar = data.photo || null
    this.company = data.company || null
    this.isAvailable = data.available
    this.isStandardGardenService = data.standard_garden_service
    // проверка на массив с id
    this.products = data.products ? data.products.map(data => typeof data === 'object' ? new Product(data) : data) : []
  }

  get minPriceForProducts() {
    if (this.products.length === 0) return null
    const prices = this.products.map(p => Number(p.price))
    return Math.min.apply(null, prices)
  }

  get tariffsForProducts() {
    if (this.products.length === 0) return []
    // преобразум в читаемый вид
    const tariffs = this.products.map(p => p.typeTariffText)
    return tariffs
  }

  get tariffsNameForProducts() {
    if (this.products.length === 0) return []
    // преобразум в читаемый вид
    const tariffsName = this.products.map(p => p.name)
    return tariffsName
  }

  static reverseMapping(data) {
    const keys = Object.keys(data)
    // заменяем название полей
    const obj = keys.reduce((acc, key) => {
      // если нет в map то не добавляем такое поле
      if (!mapAll[key]) return acc
      // только для не пустых полей
      if (!isEmpty(data[key])) acc[mapAll[key]] = data[key]
      return acc
    }, {})
    return obj
  }
}

const mapAll = {
  id: 'id',
  name: 'name',
  description: 'description',
  company: 'company',
  avatar: 'photo'
}

export const TYPES_TARIFF_ALL = {
  abonementPlanStandart: {
    value: 'abonement_plan_standart',
    text: 'Стандартное занятие по плану'
  },
  abonementFactStandart: {
    value: 'abonement_fact_standart',
    text: 'Стандартное занятие по факту'
  },
  abonementPlan: {
    value: 'abonement_plan',
    text: 'Занятие по плану'
  },
  singleProduct: {
    value: 'single_product',
    text: 'Товары и услуги'
  },
  abonementFact: {
    value: 'fact_plan',
    text: 'Занятие по факту'
  }
}
export const TYPES_TARIFF = {
  abonementPlan: {
    value: 'abonement_plan',
    text: 'Занятие по плану'
  },
  singleProduct: {
    value: 'single_product',
    text: 'Товары и услуги'
  },
  abonementFact: {
    value: 'fact_plan',
    text: 'Занятие по факту'
  }
}
export const TYPES_TARIFF_SPECIAL = {
  abonementPlanStandart: {
    value: 'abonement_plan_standart',
    text: 'Стандартное занятие по плану'
  },
  abonementFactStandart: {
    value: 'abonement_fact_standart',
    text: 'Стандартное занятие по факту'
  }
}
