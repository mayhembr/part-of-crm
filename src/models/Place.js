export class Place {
  constructor(data) {
    this.id = data.id
    this.name = data.name
    this.description = data.description || ''
    this.avatar = data.photo || null
    this.countPlaces = data.number_of_seats || 0
  }

  static reverseMapping(data) {
    const obj = {
      company: data.company,
      id: data.id,
      name: data.name,
      // photo: data.photo,
      description: data.description,
      number_of_seats: data.countPlaces
    }
    // // нельзя отправлять photo null
    if (data.photo) obj.photo = data.photo
    return obj
  }
}
