import { Account as AccountModel } from './Account'
import { KinderGarden as KinderGardenModel } from './KinderGarden'
import { isEmpty } from '../utility/helpers'

export class Children {
  constructor(data) {
    this.id = data.id
    this.firstName = data.first_name || ''
    this.lastName = data.last_name || ''
    this.birthDay = data.birth_day
    this.address = data.address
    this.additional = data.additional
    this.avatar = data.photo
    this.icon = data.icon
    this.gardenGroup = data.garden_group
    this.gardenGroupName = data.garden_group_name
    this.gardenGroupAge = data.garden_group_age
    this.family = data.family ? data.family.map(f => new AccountModel(f)) : []
    this.kindergarden = data.kindergarden ? new KinderGardenModel(data.kindergarden) : null
    this.staff = data.staff ? data.staff.map(f => new AccountModel(f)) : []
  }

  get fullName() {
    const text = []
    if (this.firstName) text.push(this.firstName)
    if (this.lastName) text.push(this.lastName)
    return text.join(' ')
  }

  static reverseMapping(data) {
    const keys = Object.keys(data)
    // заменяем название полей
    const obj = keys.reduce((acc, key) => {
      // если нет в map то не добавляем такое поле
      if (!mapAll[key]) return acc
      // только для не пустых полей
      if (!isEmpty(data[key])) acc[mapAll[key]] = data[key]
      return acc
    }, {})
    return obj
  }
}

const mapAll = {
  id: 'id',
  name: 'name',
  description: 'description',
  company: 'company',
  avatar: 'photo'
}
