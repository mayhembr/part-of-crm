export class KinderGarden {
  constructor(data) {
    this.name = data.name
    this.phone = data.phone
    this.email = data.email
    this.type = data.type
  }
}
// const TYPES_KINDER_GARDEN = {}
