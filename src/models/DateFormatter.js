import { formattedDateAndTime, formattedDeltaDate, formattedDate } from '../utility/date'

export class DateFormatter {
  constructor(startDate = '', finishDate = '') {
    this.startDate = startDate ? startDate.slice(0, 19) : ''
    this.finishDate = finishDate ? finishDate.slice(0, 19) : ''
  }

  get startDay() {
    return this.startDate.split('T')[0]
  }

  get startTime() {
    const fractions = this.startDate.split('T')
    return fractions[0] ? fractions[1].slice(0, 5) : ''
  }

  get finishDay() {
    return this.finishDate.split('T')[0]
  }

  get finishTime() {
    const fractions = this.finishDate.split('T')
    return fractions[0] ? fractions[1].slice(0, 5) : ''
  }

  get deltaDate() {
    return formattedDeltaDate(this.startDate, this.finishDate)
  }

  get formattedStartDateAndTime() {
    return formattedDateAndTime(this.startDate)
  }

  get formattedFinishDateAndTime() {
    return formattedDateAndTime(this.finishDate)
  }

  get formattedStartDate() {
    return formattedDate(this.startDate)
  }

  get formattedFinishDate() {
    return formattedDate(this.finishDate)
  }
}
