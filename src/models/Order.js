import { Product as ProductModel } from './Product'
import { Account as AccountModel } from './Account'
import { DateFormatter } from './DateFormatter'
import { Children as ChildrenModel } from './Children'
import { TYPES_TARIFF_ALL } from './Service'
import { isEmpty } from '../utility/helpers'
import { Discount as DiscountModel } from './Discount'
import { addDaysDate, setDayCurrentDate } from '../utility/date'

export class Order extends DateFormatter {
  constructor(data) {
    // Object.assign(this, data)
    super(data.start_at, data.finish_at)
    this.createdAt = data.created_at
    this.id = data.id
    this.product = data.product ? new ProductModel(data.product) : null
    this.company = data.company
    this.recipient = data.recipient ? new ChildrenModel(data.recipient) : null
    this.customer = data.customer ? new AccountModel(data.customer) : null
    this.comment = data.comment
    this.compensationTransferCosts = data.compensation_transfer_costs
    this.numberOfTransfers = data.number_of_transfers
    this.expireAt = data.expire_at
    this.isHasPayed = data.has_payed
    this.isCompleted = data.is_completed
    this.hasPayedAt = data.has_payed_at
    this.completedAt = data.completed_at
    // количество единиц услуги
    this.itemsCount = data.items_count
    this.status = data.status
    // остаток единиц услуги
    this.remainderItemsCount = data.remainder_items_count
    this.isActive = data.is_active
    this.sicknessCompensation = data.sickness_compensation
    this.reasonableCompensation = data.reasonable_compensation
    this.fixedCompensation = data.fixed_compensation
    this.billingFrequency = data.billing_frequency
    this.period = data.period
    this.payDay = data.pay_day
    this.payWithin = data.pay_within
    this.amount = data.amount
    this.expireDays = data.expire_days
    // так как с апи не приходит id заказа мы его прокидываем сверху
    this.discounts = data.discounts ? data.discounts.map(d => new DiscountModel(d, data.id)) : []

    // this.isGenerateAccount = data.is_generate_account
    this.isAutoRenewable = data.auto_renewable
  }

  get payDate() {
    if (this.product.typeTariff === TYPES_TARIFF_ALL.abonementPlan.value || this.product.typeTariff === TYPES_TARIFF_ALL.singleProduct.value || this.product.typeTariff === TYPES_TARIFF_ALL.abonementFact.value) {
      return addDaysDate(this.createdAt, this.payWithin)
    }
    if (this.product.typeTariff === TYPES_TARIFF_ALL.abonementFactStandart.value || this.product.typeTariff === TYPES_TARIFF_ALL.abonementPlanStandart.value) {
      const d = setDayCurrentDate(this.payDay)
      return addDaysDate(d, this.payWithin)
    }
    return ''
  }

  static reverseMapping(data) {
    const keys = Object.keys(data)
    // заменяем название полей
    const obj = keys.reduce((acc, key) => {
      // если нет в map то не добавляем такое поле
      if (!mapAll[key]) return acc
      // только для не пустых полей
      if (!isEmpty(data[key])) acc[mapAll[key]] = data[key]
      return acc
    }, {})
    return obj
  }

  static reverseMappingForTariff(data) {
    const keys = Object.keys(data)
    // в зависимости от тарифа выбираем подходящие поля
    let currentMap
    switch (data.typeTariff) {
      case TYPES_TARIFF_ALL.abonementPlan.value:
        currentMap = mapTariffPlan
        break
      case TYPES_TARIFF_ALL.singleProduct.value:
        currentMap = mapSingleProduct
        break
      case TYPES_TARIFF_ALL.abonementFact.value:
        currentMap = mapTariffFact
        break
      // стандартные
      case TYPES_TARIFF_ALL.abonementPlanStandart.value:
        currentMap = mapTariffSpecialPlan
        break
      case TYPES_TARIFF_ALL.abonementFactStandart.value:
        currentMap = mapTariffSpecialFact
        break
      default:
        currentMap = {}
    }
    // заменяем название полей
    const obj = keys.reduce((acc, key) => {
      // если нет в map то не добавляем такое поле
      if (!currentMap[key]) return acc
      // только для не пустых полей, и добавления false
      if (!isEmpty(data[key])) acc[currentMap[key]] = data[key]
      return acc
    }, {})
    return obj
  }
}

const mapAll = {
  id: 'id',
  company: 'company',
  startDate: 'start_at',
  finishDate: 'finish_at',
  comment: 'comment',
  expireAt: 'expire_at',
  isHasPayed: 'has_payed',
  isCompleted: 'is_completed',
  hasPayedAt: 'has_payed_at',
  completedAt: 'completed_at',
  itemsCount: 'items_count',
  isActive: 'is_active',
  sicknessCompensation: 'sickness_compensation',
  reasonableCompensation: 'reasonable_compensation',
  fixedCompensation: 'fixed_compensation',
  period: 'period',
  payDay: 'pay_day',
  payWithin: 'pay_within',
  amount: 'amount',
  productId: 'product_id',
  recipientId: 'recipient_id',
  serviceId: 'service_id',
  isGenerateAccount: 'is_generate_account',
  isAutoRenewable: 'auto_renewable',
  expireDays: 'expire_days',
  compensationTransferCosts: 'compensation_transfer_costs',
  numberOfTransfers: 'number_of_transfers'
}

const mapBase = {
  id: 'id',
  productId: 'product_id',
  recipientId: 'recipient_id',
  serviceId: 'service_id',
  company: 'company',
  amount: 'amount',

  status: 'status',

  startDate: 'start_at',
  finishDate: 'finish_at',

  payWithin: 'pay_within',

  comment: 'comment'
  // descounts
}

export const mapTariffPlan = {
  ...mapBase,
  itemsCount: 'items_count',
  expireDays: 'expire_days',

  compensationTransferCosts: 'compensation_transfer_costs',
  numberOfTransfers: 'number_of_transfers',

  isAutoRenewable: 'auto_renewable'
}

export const mapSingleProduct = {
  ...mapBase,
  expireDays: 'expire_days',
  itemsCount: 'items_count'
}

export const mapTariffFact = {
  ...mapBase,
  expireDays: 'expire_days',
  itemsCount: 'items_count'
}

export const mapTariffSpecialFact = {
  ...mapBase,
  period: 'period',
  payDay: 'pay_day'
}
export const mapTariffSpecialPlan = {
  ...mapBase,
  sicknessCompensation: 'sickness_compensation',
  reasonableCompensation: 'reasonable_compensation',
  fixedCompensation: 'fixed_compensation',
  numberOfTransfers: 'number_of_transfers',

  period: 'period',
  payDay: 'pay_day'
}
export const TYPES_STATUS_ORDER = {
  Active: {
    text: 'Активный',
    value: 'Active'
  },
  Pause: {
    text: 'Пауза',
    value: 'Pause'
  },
  Completed: {
    text: 'Завершено',
    value: 'Completed'
  }
}
