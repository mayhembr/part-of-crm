export class TeacherChip {
  constructor(data) {
    this.id = data.id
    this.firstName = data.first_name || ''
    this.lastName = data.last_name || ''
    this.email = data.email || ''

    Object.defineProperty(this, 'name', {
      get() {
        const text = []
        if (this.firstName !== '') text.push(this.firstName)
        if (this.lastName !== '') text.push(this.lastName)
        return text.join(' ')
      }
    })
  }
}
export class Teacher {
  constructor(data) {
    this.id = data.id || null
    this.email = data.email || ''
    this.firstName = data.first_name || ''
    this.lastName = data.last_name || ''
    this.language = data.language || ''
    this.phone = data.phone || ''
    this.photo = data.photo || ''
    this.icon = data.icon || ''
    this.role = data.role || ''

    Object.defineProperty(this, 'name', {
      get() {
        const text = []
        if (this.firstName !== '') text.push(this.firstName)
        if (this.lastName !== '') text.push(this.lastName)
        return text.join(' ')
      }
    })
  }
}
