import { DateFormatter } from './DateFormatter'
import { Service, TYPES_TARIFF_ALL } from './Service'
import { isEmpty } from '../utility/helpers'

export class Product extends DateFormatter {
  constructor(data) {
    super(data.start_time, data.end_time)
    this.id = data.id
    this.name = data.name
    this.description = data.description || ''
    this.price = data.price
    this.currency = data.currency
    this.typeTariff = data.tariff
    this.quantity = data.quantity
    this.expireDays = data.expire_days
    this.compenDiseaseAmount = data.compen_disease_amount
    this.compenApprovedAmount = data.compen_approved_amount
    this.fullDescription = data.full_txt_desc || ''
    this.compenFromState = data.compen_from_state
    this.compenReasonable = data.compen_reasonable
    this.isOnlyPreOrder = data.only_pre_order
    this.billingFrequency = data.billing_frequency
    this.period = data.period
    this.payDay = data.pay_day
    this.payWithin = data.pay_within
    this.tariffTitle = data.tariff_title
    this.compensationTransferCosts = data.compensation_transfer_costs
    this.numberOfTransfers = data.number_of_transfers
    this.isAutoRenewable = data.auto_renewable

    this.service = data.service ? new Service(data.service) : null
  }

  get typeTariffText() {
    if (!this.typeTariff) return ''
    const t = Object.values(TYPES_TARIFF_ALL).find(t => t.value === this.typeTariff)
    return t ? t.text : `нет совпадений для ${this.typeTariff}`
  }

  static reverseMapping(data) {
    const keys = Object.keys(data)
    // заменяем название полей
    const obj = keys.reduce((acc, key) => {
      // если нет в map то не добавляем такое поле
      if (!mapAll[key]) return acc
      // только для не пустых полей
      if (!isEmpty(data[key])) acc[mapAll[key]] = data[key]
      return acc
    }, {})
    return obj
  }

  static reverseMappingForTariff(data) {
    const keys = Object.keys(data)

    // в зависимости от тарифа выбираем подходящие поля
    let currentMap
    switch (data.typeTariff) {
      case TYPES_TARIFF_ALL.abonementPlan.value:
        currentMap = mapTariffPlan
        break
      case TYPES_TARIFF_ALL.singleProduct.value:
        currentMap = mapSingleProduct
        break
      case TYPES_TARIFF_ALL.abonementFact.value:
        currentMap = mapTariffFact
        break
      // стандартные
      case TYPES_TARIFF_ALL.abonementPlanStandart.value:
        currentMap = mapTariffSpecialPlan
        break
      case TYPES_TARIFF_ALL.abonementFactStandart.value:
        currentMap = mapTariffSpecialFact
        break
      default:
        currentMap = {}
    }
    // заменяем название полей
    const obj = keys.reduce((acc, key) => {
      // если нет в map то не добавляем такое поле
      if (!currentMap[key]) return acc
      // только для не пустых полей
      if (!isEmpty(data[key])) acc[currentMap[key]] = data[key]
      return acc
    }, {})
    return obj
  }
}
export const TYPES_PERIOD = {
  Future: {
    text: 'Будущий',
    value: 'Future'
  },
  Last: {
    text: 'Прошедший',
    value: 'Last'
  }
}

export const TYPES_BILLING_FREQUENCY = {
  Monthly: {
    text: 'Ежемесячно',
    value: 'Monthly'
  }
}
export const mapAll = {
  name: 'name',
  description: 'description',
  // поле service_id есть только для сохранения
  serviceId: 'service_id',
  // company: data.company || 203,
  price: 'price',
  currency: 'currency',
  typeTariff: 'tariff',
  quantity: 'quantity',
  expireDays: 'expire_days',
  compenDiseaseAmount: 'compen_disease_amount',
  compenApprovedAmount: 'compen_approved_amount',
  compenFromState: 'compen_from_state',
  compenReasonable: 'compen_reasonable',
  isOnlyPreOrder: 'only_pre_order',
  startDate: 'start_time',
  finishDate: 'end_time',
  period: 'period',
  payDay: 'pay_day',
  payWithin: 'pay_within',

  compensationTransferCosts: 'compensation_transfer_costs',
  numberOfTransfers: 'number_of_transfers',
  isAutoRenewable: 'auto_renewable'
}
export const mapTariffPlan = {
  id: 'id',
  name: 'name',
  // поле service_id есть только для сохранения
  serviceId: 'service_id',
  // company: data.company || 203,
  price: 'price',
  quantity: 'quantity',
  currency: 'currency',
  typeTariff: 'tariff',

  expireDays: 'expire_days',
  startDate: 'start_time',
  finishDate: 'end_time',

  compensationTransferCosts: 'compensation_transfer_costs',
  numberOfTransfers: 'number_of_transfers',

  payWithin: 'pay_within',

  isAutoRenewable: 'auto_renewable'
}
// товары и услуги
export const mapSingleProduct = {
  id: 'id',
  name: 'name',
  // поле service_id есть только для сохранения
  serviceId: 'service_id',
  // company: data.company || 203,
  price: 'price',
  quantity: 'quantity',
  currency: 'currency',
  typeTariff: 'tariff',

  startDate: 'start_time',
  finishDate: 'end_time',
  expireDays: 'expire_days',

  payWithin: 'pay_within'
}

export const mapTariffFact = {
  id: 'id',
  name: 'name',
  // поле service_id есть только для сохранения
  serviceId: 'service_id',
  // company: data.company || 203,
  price: 'price',
  quantity: 'quantity',
  currency: 'currency',
  typeTariff: 'tariff',

  startDate: 'start_time',
  finishDate: 'end_time',
  expireDays: 'expire_days',

  payWithin: 'pay_within'
}

export const mapTariffSpecialFact = {
  id: 'id',
  name: 'name',
  // поле service_id есть только для сохранения
  serviceId: 'service_id',
  // company: data.company || 203,
  price: 'price',
  currency: 'currency',
  typeTariff: 'tariff',

  period: 'period',

  payDay: 'pay_day',
  payWithin: 'pay_within'
}
export const mapTariffSpecialPlan = {
  id: 'id',
  name: 'name',
  description: 'description',
  // поле service_id есть только для сохранения
  serviceId: 'service_id',
  // company: data.company || 203,
  price: 'price',
  currency: 'currency',
  typeTariff: 'tariff',

  compenDiseaseAmount: 'compen_disease_amount',
  compenApprovedAmount: 'compen_approved_amount',
  compenFromState: 'compen_from_state',
  numberOfTransfers: 'number_of_transfers',

  period: 'period',

  payDay: 'pay_day',
  payWithin: 'pay_within'
}
