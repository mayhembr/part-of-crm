export class Account {
  constructor(data) {
    this.id = data.id
    this.email = data.email
    this.firstName = data.first_name || ''
    this.lastName = data.last_name || ''
    this.language = data.language
    this.phone = data.phone
    this.avatar = data.photo
    this.icon = data.icon
    this.role = data.role
    this.companies = data.companies || []
  }

  get fullName() {
    const text = []
    if (this.firstName) text.push(this.firstName)
    if (this.lastName) text.push(this.lastName)
    return text.join(' ')
  }
}
