import { Teacher } from './Teacher.js'
import { Place } from './Place.js'
import { Service } from './Service.js'
import { DateFormatter } from './DateFormatter'

export class Event extends DateFormatter {
  constructor(data) {
    super(data.start_at, data.finish_at)
    this.id = data.id
    // TODO разобраться с именами
    this.name = data.name || (data.service && data.service.name) || ''
    this.subject = data.subject || ''
    this.place = data.place ? new Place(data.place) : null
    this.service = data.service ? new Service(data.service) : null
    this.teachers = data.teachers ? data.teachers.map(data => new Teacher(data)) : []
    // Object.assign(this, data)
  }

  static reverseMapping(data) {
    const obj = {
      id: data.id,
      // name: data.name || null,
      subject: data.subject || '',
      start_at: data.startDate || '',
      finish_at: data.finishDate || '',
      place_id: data.placeId || '',
      teachers_id: data.teachersIds || []
      // service_id: data.serviceId || null
    }
    if (data.serviceId) obj.service_id = data.serviceId
    if (data.name) obj.name = data.name
    return obj
  }
}
