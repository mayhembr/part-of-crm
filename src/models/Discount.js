import { isEmpty } from '../utility/helpers'

export class Discount {
  constructor(data, orderId = null) {
    this.id = data.id
    this.orderId = orderId || data.order
    this.discount = data.discount
    this.discountType = data.discount_type
  }

  static reverseMapping(data) {
    const keys = Object.keys(data)
    // заменяем название полей
    const obj = keys.reduce((acc, key) => {
      // если нет в map то не добавляем такое поле
      if (!mapAll[key]) return acc
      // только для не пустых полей
      if (!isEmpty(data[key])) acc[mapAll[key]] = data[key]
      return acc
    }, {})
    return obj
  }
}

const mapAll = {
  id: 'id',
  orderId: 'order',
  discount: 'discount',
  discountType: 'discount_type'
}

export const TYPES_DISCOUNT = {
  percent: {
    value: 'percent',
    text: 'процентов'
  },
  kopeck: {
    value: 'kopeck',
    text: 'рублей'
  }
}
