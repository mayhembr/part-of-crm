export const required = v => !!v || 'Это поле обязательно'
export const requiredSelect = v => !!v || 'Выбор в этом поле обязателен'
export const maxLength = v => (v && v.length <= 10) || 'Это поле должно быть не более 10 символов'
// если не чего не передали это тоже валидно
export const onlyInt = n => !n || (Number(n) === n && n % 1 === 0) || 'Это поле для целых чисел'
export const onlyPositive = n => {
  // Number(n.toString()) это отбрасываем нули в конце числа
  // replace(/[,]/g, '.') а это для допуска и запятых
  const res = !n || (Number((n.toString()).replace(/[,]/g, '.')) === Number(n.toString()) && n >= 0) || 'Это поле для положительных чисел'
  return res
}
// function isFloat(n){
//   return Number(n) === n && n % 1 !== 0;
// }
export const creatorMinLength = n => {
  return v => (!!v && v.length >= n) || 'Минимальное количество символов ' + n
}
export const creatorMaxLength = n => {
  return v => (!!v && v.length <= n) || 'Максимальное количество символов ' + n
}
