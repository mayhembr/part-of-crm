import { format, max, min, addDays, setDate, eachDayOfInterval, endOfWeek, endOfMonth, differenceInCalendarDays, formatISO } from 'date-fns'
import ru from 'date-fns/locale/ru'
const TEMPLATION_DATE = 'dd MMMM yyyy'
const TEMPLATION_TIME = 'HH:mm'

/**
 * @param {string} startDate
 * @param {string} endDate
 */
export function formattedDeltaDate(startDate, endDate) {
  if (!startDate || !endDate) return ''
  const start = new Date(startDate)
  const end = new Date(endDate)
  const fullDateStart = format(start, TEMPLATION_DATE, { locale: ru })
  const fullDateEnd = format(end, TEMPLATION_DATE, { locale: ru })
  const timeStart = format(start, TEMPLATION_TIME, { locale: ru })
  const timeEnd = format(end, TEMPLATION_TIME, { locale: ru })

  if (fullDateStart === fullDateEnd) {
    return `с ${timeStart} по ${timeEnd}, ${fullDateStart} года`
  } else {
    return `с ${fullDateStart}, ${timeStart} по ${fullDateEnd}, ${timeEnd}`
  }
}

export function formattedDate(d) {
  if (!d) return ''
  const date = new Date(d)
  const fullDate = format(date, TEMPLATION_DATE, { locale: ru })
  return fullDate
}

export function formattedDateAndTime(d) {
  if (!d) return ''
  const date = new Date(d)
  const fullDate = format(date, TEMPLATION_DATE, { locale: ru })
  const time = format(date, TEMPLATION_TIME, { locale: ru })

  return `${fullDate}г. ${time}`
}
export function minFromDates(dates) {
  dates = dates.filter(d => !!d)
  dates = dates.map(d => new Date(d))
  const result = min(dates)
  return result
}
export function maxFromDates(dates) {
  dates = dates.filter(d => !!d)
  dates = dates.map(d => new Date(d))
  return dates.length ? max(dates) : ''
}
export function addDaysDate(date, days = 0) {
  if (!date) return ''
  const d = new Date(date)
  const result = addDays(d, days)
  // return result.toISOString().split('T')[0]
  return result
}
export function setDayCurrentDate(day = 1) {
  const d = new Date()
  const result = setDate(d, day)
  return result
}
export function daysOfInterval(startDate = '', endDate = '') {
  if (!startDate || !endDate) return []

  const start = new Date(startDate)
  const end = new Date(endDate)

  const result = eachDayOfInterval({ start, end })
  return result
}
export function endWeek(startDate = new Date()) {
  if (!startDate) return []

  const start = new Date(startDate)

  const result = endOfWeek(start, { weekStartsOn: 1 })
  return result
}
export function endMonth(startDate = new Date()) {
  if (!startDate) return []

  const start = new Date(startDate)

  const result = endOfMonth(start)
  return result
}
export function deltaInCalendarDays(startDate = '', endDate = '') {
  if (!startDate || !endDate) return 0

  const start = new Date(startDate)
  const end = new Date(endDate)
  const result = differenceInCalendarDays(start, end)
  return result
}
export function formatISOString(date) {
  if (!date) return ''

  const start = new Date(date)
  const result = formatISO(start)
  return result
}
