/**
 * @param {Object} obj
 */
export function convertObjToFormData(obj) {
  const formData = new FormData()
  const keys = Object.keys(obj)
  for (const key of keys) {
    formData.append(key, obj[key])
  }
  return formData
}

/**
 * @param {Object} obj
 */
// для добавления учителей нужно по специальному добавлять их
export function convertObjToFormDataForLesson(obj) {
  const formData = new FormData()
  const keys = Object.keys(obj)
  for (const key of keys) {
    if (key === 'teachers_id') {
      for (const id of obj.teachers_id) {
        formData.append('teachers_ids', id)
      }
    } else formData.append(key, obj[key])
  }
  return formData
}

/**
 * @param {String} str
 */
export function isBase64(str) {
  try {
    return str.indexOf('base64') !== -1
  } catch (e) {
    return false
  }
}

export function isEmpty(val) {
  return (typeof val === 'undefined' || val === null || val === '')
}
