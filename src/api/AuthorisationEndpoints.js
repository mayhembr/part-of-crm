import axios from '../config/axiosConfig'

const baseURL = '/api/account/'

export async function getAccount() {
  const { data } = await axios.get(baseURL)
  return data
}

export async function getToken() {
  const { data: { token } } = await axios.get(baseURL + 'token/', { withCredentials: true })
  return token
}
