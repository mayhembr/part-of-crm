import axios from '../config/axiosConfig'
import { convertObjToFormData } from '../utility/helpers'

const baseURL = '/api/crm/service/'

export async function get(company = null) {
  const { data: { results } } = await axios.get(baseURL, { params: { company } })
  return results
}

export async function add(service) {
  const formData = convertObjToFormData(service)
  const { data } = await axios.post(baseURL, formData)
  return data
}
export async function update(service) {
  const formData = convertObjToFormData(service)
  const { data } = await axios.put(`${baseURL}${service.id}/`, formData)
  return data
}

export async function deleteService(id) {
  const { data: { results } } = await axios.delete(baseURL + id + '/')
  return results
}
