import axios from '../config/axiosConfig'

const baseURL = '/api/garden/children/'

export async function getGrouped() {
  const { data: { results } } = await axios.get(baseURL)
  return results
}
// часть запросов для детей находится в EventsEndpoints
