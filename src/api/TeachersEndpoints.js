import axios from '../config/axiosConfig'

const baseURL = '/api/garden/teachers/'

export async function get() {
  const { data: { results } } = await axios.get(baseURL)
  return results
}
