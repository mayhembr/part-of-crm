import axios from '../config/axiosConfig'
import { convertObjToFormData } from '../utility/helpers'

const baseURL = '/api/crm/invoice/'
const invoiceitemURL = '/api/crm/invoiceitem/'

export async function get() {
  const { data: { results } } = await axios.get(baseURL)
  return results
}

export async function add(invoice) {
  const formData = convertObjToFormData(invoice)
  const { data } = await axios.post(baseURL, formData)
  return data
}
export async function update(invoice) {
  const formData = convertObjToFormData(invoice)
  const { data } = await axios.patch(`${baseURL}${invoice.id}/`, formData)
  return data
}
export async function addItem(invoiceItem) {
  const formData = convertObjToFormData(invoiceItem)
  const { data } = await axios.post(`${baseURL}${invoiceItem.invoice}/item-add/`, formData)
  return data
}
export async function updateItem(invoiceItem) {
  const formData = convertObjToFormData(invoiceItem)
  const { data } = await axios.patch(`${invoiceitemURL}${invoiceItem.id}/`, formData)
  return data
}
export async function deleteItem(id) {
  const { data: { results } } = await axios.delete(invoiceitemURL + id + '/')
  return results
}
export async function getInvoice(id) {
  const { data: { results } } = await axios.get(baseURL + id + '/')
  return results
}
export async function paid(id) {
  const formData = convertObjToFormData(id)
  const { data: { results } } = await axios.post(baseURL + 'paid/', formData)
  return results
}
export async function send(id) {
  const { data: { results } } = await axios.get(baseURL + id + '/send/')
  return results
}

export async function deleteInvoice(id) {
  const { data: { results } } = await axios.delete(baseURL + id + '/')
  return results
}
