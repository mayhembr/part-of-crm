import axios from '../config/axiosConfig'
import { convertObjToFormData } from '../utility/helpers'

const baseURL = '/api/crm/product/'

export async function get() {
  const { data: { results } } = await axios.get(baseURL)
  return results
}

export async function add(tariff) {
  const formData = convertObjToFormData(tariff)
  const { data } = await axios.post(baseURL, formData)
  return data
}

export async function update(tariff) {
  const formData = convertObjToFormData(tariff)
  const { data } = await axios.put(`${baseURL}${tariff.id}/`, formData)
  return data
}

export async function deleteProduct(id) {
  const { data: { results } } = await axios.delete(baseURL + id + '/')
  return results
}
