import axios from '../config/axiosConfig'
import { convertObjToFormData } from '../utility/helpers'

const baseURL = '/api/crm/order/'
const discountURL = baseURL + 'discount/'

export async function get() {
  const { data: { results } } = await axios.get(baseURL)
  return results
}

export async function add(opder) {
  const formData = convertObjToFormData(opder)
  const { data } = await axios.post(baseURL, formData)
  return data
}

export async function update(opder) {
  const formData = convertObjToFormData(opder)
  const { data } = await axios.put(`${baseURL}${opder.id}/`, formData)
  return data
}

export async function deleteOpder(id) {
  const { data: { results } } = await axios.delete(baseURL + id + '/')
  return results
}

export async function addDiscount(discount) {
  const formData = convertObjToFormData(discount)
  const { data } = await axios.post(discountURL, formData)
  return data
}

export async function updateDiscount(discount) {
  const formData = convertObjToFormData(discount)
  const { data } = await axios.put(`${discountURL}${discount.id}/`, formData)
  return data
}

export async function deleteDiscount(id) {
  const { data: { results } } = await axios.delete(discountURL + id + '/')
  return results
}
