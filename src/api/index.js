import * as events from './EventsEndpoints'
import * as auth from './AuthorisationEndpoints'
import * as services from './ServicesEndpoints'
import * as places from './PlacesEndpoints'
import * as teachers from './TeachersEndpoints'
import * as childrens from './ChildrenEndpoints'
import * as products from './ProductEndpoints'
import * as orders from './OrdersEndpoints'
import * as invoices from './InvoicesEndpoints'
import * as finances from './FinancesEndpoints'

// TODO поменять создание url на "new URL()"
export default { auth, events, services, places, teachers, childrens, products, orders, invoices, finances }
