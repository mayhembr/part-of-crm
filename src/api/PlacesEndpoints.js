import axios from '../config/axiosConfig'
import { convertObjToFormData } from '../utility/helpers'

const baseURL = '/api/crm/place/'

export async function get() {
  const { data: { results } } = await axios.get(baseURL)
  return results
}

export async function update(place) {
  const formData = convertObjToFormData(place)
  const { data } = await axios.put(`${baseURL}${place.id}/`, formData)
  return data
}

export async function add(place) {
  const formData = convertObjToFormData(place)
  const { data } = await axios.post(baseURL, formData)
  return data
}
export async function deletePlace(id) {
  const { data: { results } } = await axios.delete(baseURL + id + '/')
  return results
}
