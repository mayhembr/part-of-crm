import axios from '../config/axiosConfig'
// import { convertObjToFormData } from '../utility/helpers'

const baseURL = '/api/crm/finance/'

export async function get() {
  const { data: { results } } = await axios.get(baseURL)
  return results
}

export async function getForChildren(id) {
  const { data: { results } } = await axios.get(baseURL + id + '/')
  return results
}
