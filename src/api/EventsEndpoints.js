import axios from '../config/axiosConfig'
import { convertObjToFormData, convertObjToFormDataForLesson } from '../utility/helpers'

const baseURL = '/api/crm/event/'
const childrenURL = idEvt => `/api/crm/event/${idEvt}/children/`

export async function get() {
  const { data: { results } } = await axios.get(baseURL)
  return results
}

export async function add(event) {
  const formData = convertObjToFormDataForLesson(event)
  const { data } = await axios.post(baseURL, formData)
  return data
}

export async function update(event) {
  const formData = convertObjToFormDataForLesson(event)
  const { data } = await axios.put(`${baseURL}${event.id}/`, formData)
  return data
}

export async function deleteEvent(id) {
  const { data: { results } } = await axios.delete(baseURL + id + '/')
  return results
}

// запросы для учеников на занятии
export async function getChildren(idEvent) {
  const { data: { results } } = await axios.get(childrenURL(idEvent))
  return results
}
export async function saveAttendance(attendance) {
  const formData = convertObjToFormData(attendance)
  const { data } = await axios.post(baseURL + 'attendance/', formData)
  return data
}
export async function updateAttendance(attendance) {
  const formData = convertObjToFormData(attendance)
  const { data } = await axios.put(baseURL + 'attendance/', formData)
  return data
}
